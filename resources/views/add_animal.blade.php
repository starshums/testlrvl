@extends('app')

@section('content')
<div class="row">

<form class="form" action="{{ route('save.animal') }}" method="post">
{{ csrf_field() }}

<h2> Add an Animal </h2>

  <div class="form-group" >
    <label for="animal_name">Animal Name</label>
    <input type="text" class="form-control" name="animal_name" id="animal_name" placeholder="Enter animal name">
  </div>

  <div class="form-group">
    <label for="animal_mood">Animal Mood</label>
    <input type="text" class="form-control" name="animal_mood" id="animal_mood" placeholder="Enter animal mood">
  </div>

  <button type="submit" class="btn btn-primary">Add Animal</button>

</form>


</div>
@endsection