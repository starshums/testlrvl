@extends('app')

@section('content')
<div class="row">

    <hr><br>
    <h2> Animals <a href="{{url('/add-animal')}}" class="btn btn-primary">Add a new Animal</a> </h2>
    <hr>
    <br>
    <form class="container form" method="post" action="{{ route('search.animal') }}">
    {{ csrf_field() }}
        <div class="form-inline container">
            <input class="form-control col-sm-11" type="search" name="search_query" placeholder="Search for an animal">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit"> Search</button>
        </div>
    </form>
    <hr>

    <table class="table m-5">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Mood</th>
        </thead>
        <tbody>
            @foreach( $animals as $animal)
                <tr>
                    <td>{{ $animal->id_animal }}</td>
                    <td>{{ $animal->name }}</td>
                    <td>{{ $animal->mood }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>
@endsection