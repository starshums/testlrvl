<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    protected $table = 'animals';
    protected $primaryKey = "id_animal";
    protected $fillable = [
        "name",
        "mood"
    ];
}
