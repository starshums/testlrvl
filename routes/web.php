<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    $animals = App\Animal::all();
    return view('welcome', compact('animals'));
});

Route::get('/add-animal', function() {
    return view('add_animal');
});

Route::post('save-animal', function(Request $request) {
    $animal = new App\Animal;
    $animal->name = $request->get('animal_name');
    $animal->mood = $request->get('animal_mood');
    $animal->save();
    return \Redirect::to('/');
})->name('save.animal');

Route::post('search-animal', function(Request $request) {
    $query = $request->get("search_query");
    $animals = App\Animal::where("name", "LIKE", "%" . $query . "%")
    ->orWhere("mood", "LIKE", "%" . $query . "%")->get();
    return view('welcome', compact('animals'));
})->name('search.animal');
